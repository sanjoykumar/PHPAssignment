/*

Definition and Usage:
--------------------

The reset() function moves the internal pointer to the first element of the array.

Related methods:

    current() - returns the value of the current element in an array
    end() - moves the internal pointer to, and outputs, the last element in the array
    next() - moves the internal pointer to, and outputs, the next element in the array
    prev() - moves the internal pointer to, and outputs, the previous element in the array
    each() - returns the current element key and value, and moves the internal pointer forward

Syntax:
------
reset(array)

Parameter 	Description
---------------------------
array   	Required. Specifies the array to use

Return Value: 	
------------
Returns the value of the first element in the array on success, or FALSE if the array is empty


*/


<!DOCTYPE html>
<html>
    <body>

        <?php
        $people = array("Peter", "Joe", "Glenn", "Cleveland");

        echo current($people) . "<br>";
        echo next($people) . "<br>";

        echo reset($people);
        ?>

    </body>
</html>