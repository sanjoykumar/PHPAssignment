/*

Definition and Usage:
--------------------

The sizeof() function returns the number of elements in an array.

The sizeof() function is an alias of the count() function. 

Syntax:
------
sizeof(array,mode);



Parameter 	Description
---------------------------
array           Required. Specifies the array
mode            Optional. Specifies the mode. Possible values:

0 - Default. Does not count all elements of multidimensional arrays
1 - Counts the array recursively (counts all the elements of multidimensional arrays)


Return Value: 	
------------

Returns the number of elements in the array




*/

<!DOCTYPE html>
<html>
    <body>

        <?php
        $cars = array("Volvo", "BMW", "Toyota");
        echo sizeof($cars);
       
        //Count the array recursively:
        $carsrecur = array
            (
            "Volvo" => array
                (
                "XC60",
                "XC90"
            ),
            "BMW" => array
                (
                "X3",
                "X5"
            ),
            "Toyota" => array
                (
                "Highlander"
            )
        );

        echo "Normal count: " . sizeof($carsrecur) . "<br>";
        echo "Recursive count: " . sizeof($carsrecur, 1);
        ?>


    </body>
</html>