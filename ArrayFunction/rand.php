/*

Definition and Usage:

The rand() function generates a random integer.

Tip: If you want a random integer between 10 and 100 (inclusive), use rand (10,100).

Tip: The mt_rand() function produces a better random value, and is 4 times faster than rand().


Syntax:

rand();

or

rand(min,max);

*/



<?php

echo(rand() . "<br>");
echo(rand() . "<br>");
echo(rand(10, 100));
?> 