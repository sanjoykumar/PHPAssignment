/*

Definition and Usage:

The key() function returns the element key from the current internal pointer position.

This function returns FALSE on error.


Syntax:

key(array)

Parameter 	Description
---------------------------

array 	Required. Specifies the array to use


Return Value: 	

Returns the key of the array element that is currently being pointed to by the internal pointer

*/



<!DOCTYPE html>
<html>
    <body>

        <?php
        $people = array("Peter", "Joe", "Glenn", "Cleveland");
        echo "The key from the current position is: " . key($people);
        ?>

    </body>
</html>