/*

Definition and Usage:

The compact() function creates an array from variables and their values.

Note: Any strings that does not match variable names will be skipped.

Syntax:


compact(var1,var2...) 

*/

<?php
$firstname = "Peter";
$lastname = "Griffin";
$age = "41";

$result = compact("firstname", "lastname", "age");

print_r($result);
?> 



<?php
$firstname = "Peter";
$lastname = "Griffin";
$age = "41";

$name = array("firstname", "lastname");
$result = compact($name, "location", "age");

print_r($result);
?> 


