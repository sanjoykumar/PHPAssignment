/*


Definition and Usage:

The list() function is used to assign values to a list of variables in one operation.

Note: This function only works on numerical arrays.

Syntax:

list(var1,var2...)

Parameter 	Description
---------------------------
var1 	Required. The first variable to assign a value to
var2,... 	Optional. More variables to assign values to

Return Value: 	

Returns the assigned array

*/

<!DOCTYPE html>
<html>
    <body>

        <?php
        $my_array = array("Dog", "Cat", "Horse");

        list($a, $b, $c) = $my_array;
        echo "I have several animals, a $a, a $b and a $c.";

        //Using the first and third variables:
        
        $my_array2 = array("Dog", "Cat", "Horse");

        list($a2,, $c2) = $my_array2;
        echo "Here I only use the $a2 and $c2 variables.";
        ?>

    </body>
</html>