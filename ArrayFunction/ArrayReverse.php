/*

Definition and Usage:

The array_reverse() function returns an array in the reverse order.


Syntax:

array_reverse(array,preserve) 


Return Value: 	

Returns the reversed array

*/

<?php
$a=array("a"=>"Volvo","b"=>"BMW","c"=>"Toyota");
print_r(array_reverse($a));
?> 