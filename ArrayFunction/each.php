/*

Definition and Usage:

The each() function returns the current element key and value, and moves the internal pointer forward.

This element key and value is returned in an array with four elements. 
Two elements (1 and Value) for the element value, and two elements (0 and Key) for the element key.

Related methods:

current() - returns the value of the current element in an array
end() - moves the internal pointer to, and outputs, the last element in the array
next() - moves the internal pointer to, and outputs, the next element in the array
prev() - moves the internal pointer to, and outputs, the previous element in the array
reset() - moves the internal pointer to the first element of the array

Syntax:


each(array) 


Return Value: 	

Returns the current element key and value. 
This element key and value is returned in an array with four elements. 
Two elements (1 and Value) for the element value, and two elements (0 and Key) for the element key. 
This function returns FALSE if there are no more array elements


*/

<?php
$people = array("Peter", "Joe", "Glenn", "Cleveland");
print_r(each($people));
?> 



<!DOCTYPE html>
<html>
    <body>

        <?php
        $people = array("Peter", "Joe", "Glenn", "Cleveland");

        reset($people);

        while (list($key, $val) = each($people)) {
            echo "$key => $val<br>";
        }
        ?>

    </body>
</html>