/*

Defination:  array_keys — Return all the keys or a subset of the keys of an array.

Syntex: array array_keys ( array $array [, mixed $search_value [, bool $strict = false ]] )


array_keys() returns the keys, numeric and string, from the array. 

Return: Returns an array of all the keys in array. 


*/




<?php

$array = array(0 => 100, "color" => "red");
print_r(array_keys($array));

$array = array("blue", "red", "green", "blue", "blue");
print_r(array_keys($array, "blue"));

$array = array("color" => array("blue", "red", "green"),
    "size" => array("small", "medium", "large"));
print_r(array_keys($array));
?>
