/*

Definition and Usage:
--------------------

The pos() function returns the value of the current element in an array.

This function is an alias of the current() function.

Every array has an internal pointer to its "current" element, which is initialized to the first element inserted into the array.

Tip: This function does not move the arrays internal pointer.

Related methods:

    current() - returns the value of the current element in an array
    end() - moves the internal pointer to, and outputs, the last element in the array
    next() - moves the internal pointer to, and outputs, the next element in the array
    prev() - moves the internal pointer to, and outputs, the previous element in the array
    reset() - moves the internal pointer to the first element of the array
    each() - returns the current element key and value, and moves the internal pointer forward

Syntax:
-------

pos(array)

Parameter 	Description
---------------------------
array:   	Required. Specifies the array to use

Return Value: 	

Returns the value of the current element in an array, or FALSE on empty elements or elements with no value


*/


<!DOCTYPE html>
<html>
    <body>

        <?php
        $people = array("Peter", "Joe", "Glenn", "Cleveland");

        echo pos($people) . "<br>";
        ?>

    </body>
</html>