/*

Definition and Usage:

The count() function returns the number of elements in an array.


Syntax:

count(array,mode);



Parameter 	Description
----------------------------
array 	Required. Specifies the array
mode 	Optional. Specifies the mode. Possible values:

0 - Default. Does not count all elements of multidimensional arrays
1 - Counts the array recursively (counts all the elements of multidimensional arrays)



*/




<!DOCTYPE html>
<html>
    <body>

        <?php
        $cars = array
            (
            "Volvo" => array
                (
                "XC60",
                "XC90"
            ),
            "BMW" => array
                (
                "X3",
                "X5"
            ),
            "Toyota" => array
                (
                "Highlander"
            )
        );
        echo "Normal count: " . count($cars) . "<br>";
        echo "Recursive count: " . count($cars, 1);
        ?>

    </body>
</html>