/*

Definition and Usage:

The array_push() function inserts one or more elements to the end of an array.

Tip: You can add one value, or as many as you like.

Note: Even if your array has string keys, your added elements will always have numeric keys (See example below).


Syntax:

array_push(array,value1,value2...) 

*/


<?php

$a = array("red", "green");
array_push($a, "blue", "yellow");
print_r($a);
?> 