/*

Definition and Usage:
--------------------

The prev() function moves the internal pointer to, and outputs, the previous element in the array.

Related methods:

    next() - moves the internal pointer to, and outputs, the next element in the array
    current() - returns the value of the current element in an array
    end() - moves the internal pointer to, and outputs, the last element in the array
    reset() - moves the internal pointer to the first element of the array
    each() - returns the current element key and value, and moves the internal pointer forward

Syntax:
------

prev(array)

Parameter 	Description
---------------------------
array:  	Required. Specifies the array to use


Return Value: 	
------------
Returns the value of the previous element in the array on success, or FALSE if there are no more elements


*/


<!DOCTYPE html>
<html>
    <body>

        <?php
        $people = array("Peter", "Joe", "Glenn", "Cleveland");

        echo current($people) . "<br>";
        echo next($people) . "<br>";
        echo prev($people);
        ?>

    </body>
</html>