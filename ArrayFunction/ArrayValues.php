/*

Definition and Usage:


The array_values() function returns an array containing all the values of an array.

Tip: The returned array will have numeric keys, starting at 0 and increase by 1.


Syntax:


array_values(array) 


Return Value: 	

Returns an array containing all the values of an array



*/


<?php
$a=array("Name"=>"Peter","Age"=>"41","Country"=>"USA");
print_r(array_values($a));
?> 