
/*

Definition and Usage

The str_pad() function pads a string to a new length.


Syntax:

str_pad(string,length,pad_string,pad_type) 




*/







// STR_PAD_RIGHT - Pad to the right side of the string. This is default

<?php
$str = "Hello World";
echo str_pad($str,20,".");
?>

//Pad to the left side of the string

<?php
$str = "Hello World";
echo str_pad($str,20,".",STR_PAD_LEFT);
?>


//Pad to both sides of the string

<?php
$str = "Hello World";
echo str_pad($str,20,".:",STR_PAD_BOTH);
?>


