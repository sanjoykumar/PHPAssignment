


//PHP Indexed Arrays

<?php
$cars = array("Volvo", "BMW", "Toyota");
echo "I like " . $cars[0] . ", " . $cars[1] . " and " . $cars[2] . ".";
?> 

//Get The Length of an Array - The count() Function


<?php
$cars = array("Volvo", "BMW", "Toyota");
echo count($cars);
?> 



//Loop Through an Indexed Array


<?php
$cars = array("Volvo", "BMW", "Toyota");
$arrlength = count($cars);

for ($x = 0; $x < $arrlength; $x++) {
    echo $cars[$x];
    echo "<br>";
}
?> 


/*

PHP Associative Arrays

Associative arrays are arrays that use named keys that you assign to them.

There are two ways to create an associative array: 


$age = array("Peter"=>"35", "Ben"=>"37", "Joe"=>"43");

or:


$age['Peter'] = "35";
$age['Ben'] = "37";
$age['Joe'] = "43"; 



*/

<?php
$age = array("Peter" => "35", "Ben" => "37", "Joe" => "43");
echo "Peter is " . $age['Peter'] . " years old.";
?> 




//Loop Through an Associative Array



<?php
$age = array("Peter" => "35", "Ben" => "37", "Joe" => "43");

foreach ($age as $x => $x_value) {
    echo "Key=" . $x . ", Value=" . $x_value;
    echo "<br>";
}
?>



/*


PHP - Multidimensional Arrays

A multidimensional array is an array containing one or more arrays.

PHP understands multidimensional arrays that are two, three, four, five, or more levels deep. 
However, arrays more than three levels deep are hard to manage for most people.

Note 	The dimension of an array indicates the number of indices you need to select an element.


For a two-dimensional array you need two indices to select an element
For a three-dimensional array you need three indices to select an element

*/



<?php
$cars = array
    (
    array("Volvo", 22, 18),
    array("BMW", 15, 13),
    array("Saab", 5, 2),
    array("Land Rover", 17, 15)
);

for ($row = 0; $row < 4; $row++) {
    echo "<p><b>Row number $row</b></p>";
    echo "<ul>";
    for ($col = 0; $col < 3; $col++) {
        echo "<li>" . $cars[$row][$col] . "</li>";
    }
    echo "</ul>";
}
?>





