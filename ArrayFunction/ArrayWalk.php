/*

Definition and Usage:


The array_walk() function runs each array element in a user-defined function. The array's keys and values are parameters in the function.

Note: You can change an array element's value in the user-defined function by specifying the first parameter as a reference: &$value (See Example 2).

Tip: To work with deeper arrays (an array inside an array), use the array_walk_recursive() function.


Syntax:


array_walk(array,myfunction,parameter...) 


Return Value: 	

Returns TRUE on success or FALSE on failure


*/




<?php
function myfunction($value,$key)
{
echo "The key $key has the value $value<br>";
}
$a=array("a"=>"red","b"=>"green","c"=>"blue");
array_walk($a,"myfunction");
?> 




<?php
function myfunctionp($value,$key,$p)
{
echo "$key $p $value<br>";
}
$a=array("a"=>"red","b"=>"green","c"=>"blue");
array_walk($a,"myfunctionp","has the value");
?> 

