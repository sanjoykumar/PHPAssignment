//---------------------------Defination---------------------------------------------------
/*

Defination: array_key_exists — Checks if the given key or index exists in the array

Syntex: 

bool array_key_exists ( mixed $key , array $array )


array_key_exists() returns TRUE if the given key is set in the array. key can be any value possible for an array index. 


Return Value:

Returns TRUE on success or FALSE on failure. 

*/



//---------------------------------------Code Start ---------------------------------------




<?php

$search_array = array('first' => 1, 'second' => 4);
if (array_key_exists('first', $search_array)) {
    echo "The 'first' element is in the array";
}
?>
