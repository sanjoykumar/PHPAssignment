/*

Definition and Usage:
--------------------

The sort() function sorts an indexed array in ascending order.

Tip: Use the rsort() function to sort an indexed array in descending order.

Syntax:
------
sort(array,sortingtype);

Parameter 	Description
---------------------------
array           Required. Specifies the array to sort
sortingtype 	Optional. Specifies how to compare the array elements/items. Possible values:

0 = SORT_REGULAR - Default. Compare items normally (don't change types)
1 = SORT_NUMERIC - Compare items numerically
2 = SORT_STRING - Compare items as strings
3 = SORT_LOCALE_STRING - Compare items as strings, based on current locale
4 = SORT_NATURAL - Compare items as strings using natural ordering
5 = SORT_FLAG_CASE -


Return Value: 	
------------

TRUE on success. FALSE on failure



*/


<!DOCTYPE html>
<html>
    <body>

        <?php
        $cars = array("Volvo", "BMW", "Toyota");
        sort($cars);

        $clength = count($cars);
        for ($x = 0; $x < $clength; $x++) {
            echo $cars[$x];
            echo "<br>";
        }

        //Sort the elements of the $numbers array in ascending numerical order:

        $numbers = array(4, 6, 2, 22, 11);
        sort($numbers);
        ?>

    </body>
</html>