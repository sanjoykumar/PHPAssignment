/*

Definition and Usage:
--------------------

The shuffle() function randomizes the order of the elements in the array.

This function assigns new keys for the elements in the array. Existing keys will be removed (See Example 1 below).



Syntax:
------
shuffle(array)

Parameter 	Description
---------------------------
array    	Required. Specifies the array to use

Return Value:
------------
Returns TRUE on success or FALSE on failure



*/


<!DOCTYPE html>
<html>
    <body>

        <?php
        $my_array = array("red", "green", "blue", "yellow", "purple");

        shuffle($my_array);
        print_r($my_array);
        ?>

        <p>Refresh the page to see how shuffle() randomizes the order of the elements in the array.</p>

    </body>
</html>