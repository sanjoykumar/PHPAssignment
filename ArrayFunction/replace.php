/*

Definition and Usage:

The str_replace() function replaces some characters with some other characters in a string.

This function works by the following rules:

    If the string to be searched is an array, it returns an array
    If the string to be searched is an array, find and replace is performed with every array element
    If both find and replace are arrays, and replace has fewer elements than find, an empty string will be used as replace
    If find is an array and replace is a string, the replace string will be used for every find value

Note: This function is case-sensitive. Use the str_ireplace() function to perform a case-insensitive search.

Note: This function is binary-safe.



Syntax:

str_replace(find,replace,string,count) 


*/

<?php
echo str_replace("world","Peter","Hello world!");
?>


<?php
$arr = array("blue","red","green","yellow");
print_r(str_replace("red","pink",$arr,$i));
echo "<br>" . "Replacements: $i";
?>


