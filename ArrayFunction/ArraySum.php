/*

Definition and Usage:

The array_sum() function returns the sum of all the values in the array.

Syntax:

array_sum(array)


Return Value: 	


Returns the sum of all the values in an array


*/

<?php
$a=array(5,15,25);
echo array_sum($a);
?> 


<?php
$a=array("a"=>52.2,"b"=>13.7,"c"=>0.9);
echo array_sum($a);
?> 


