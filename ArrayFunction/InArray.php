/*

Definition and Usage:

The in_array() function searches an array for a specific value.

Note: If the search parameter is a string and the type parameter is set to TRUE, the search is case-sensitive.


Syntax:

in_array(search,array,type)

Parameter 	Description
---------------------------

search: 	Required. Specifies the what to search for
array :  	Required. Specifies the array to search
type:   	Optional. If this parameter is set to TRUE, the in_array() function searches for the search-string and specific type in the array.


Return Value: 	

Returns TRUE if the value is found in the array, or FALSE otherwise



*/





<!DOCTYPE html>
<html>
    <body>

        <?php
        $people = array("Peter", "Joe", "Glenn", "Cleveland", 23);

        if (in_array("23", $people, TRUE)) {
            echo "Match found<br>";
        } else {
            echo "Match not found<br>";
        }
        if (in_array("Glenn", $people, TRUE)) {
            echo "Match found<br>";
        } else {
            echo "Match not found<br>";
        }

        if (in_array(23, $people, TRUE)) {
            echo "Match found<br>";
        } else {
            echo "Match not found<br>";
        }
        ?>

    </body>
</html>